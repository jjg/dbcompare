﻿using System.Windows;
using Dasic.Controls;

namespace Dasic
{
    /// <summary>
    ///     MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        /// <summary>
        ///     运行数据库对比工具
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDbCompare_OnClick(object sender, RoutedEventArgs e)
        {
            var dbCompare = new DbCompare();
            dbCompare.Show();
        }
    }
}