﻿using System.Windows.Controls;
using System.Windows.Media;

namespace Dasic.Library
{
    internal class TreeViewParent : TreeViewItem
    {
        private readonly Image _expandImage = new Image();

        private readonly Image _icoImage = new Image();

        public ImageSource IcoImage
        {
            get { return _icoImage.Source; }
            set { _icoImage.Source = value; }
        }


        public ImageSource ExpandImage
        {
            get { return _expandImage.Source; }
            set { _expandImage.Source = value; }
        }
    }
}